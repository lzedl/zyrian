# -*- coding: utf8 -*-
from argparse import ArgumentParser
from zyrian.core import Zyrian
import logging.config
import yaml


def main():
    parser = ArgumentParser()
    parser.add_argument('--network', action='store_true', default=False)
    args = parser.parse_args()

    with open('config.yml') as fd:
        config = yaml.load(fd)
        logging.config.dictConfig(config['logging'])

    logger = logging.getLogger('zyrian')

    if config.get('disable_gc', False):
        import gc
        logger.info('Disable garbage collecting')
        gc.disable()

    core = Zyrian()
    core.run(args.network)

if __name__ == '__main__':
    main()
