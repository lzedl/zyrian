# -*- coding: utf8 -*-
from gamepy.drawable import Drawable
from zyrian.locals import ENEMIES
from zyrian.ship import Ship
from zyrian import g


class Asteroid(Drawable):
    damage = 5
    sprites = {
        1: 'asteroid-s',
        2: 'asteroid-m',
        3: 'asteroid-l',
    }
    side = ENEMIES

    def __init__(self, x, y, size, speed=200):
        """Asteroid constructor

        :param x:
        :param y:
        :param int size: size from 1 to 3
        """
        from gamepy.core import GameCore

        sprite = GameCore.instance.storage.sprites[self.sprites[size]]
        if size == 1:
            self.hp = 4
        elif size == 2:
            self.hp = 10
        elif size == 3:
            self.hp = 20

        self.speed = speed

        super(Asteroid, self).__init__(x, y,
                                       int(sprite.w*0.8),
                                       int(sprite.h*0.8),
                                       sprite)

    def update(self, core):
        self.y += self.speed * g.deltatime

        for obj in core.collisions_for(self):
            if isinstance(obj, Ship) and self.side != obj.side:
                obj.hit(self.damage)
                # core.objects.remove(self)
                self.remove = True
                return

        if self.y > core.screen_height + self.sprite.h:
            if self in core.objects:
                # core.objects.remove(self)
                self.remove = True

    def hit(self, damage):
        from gamepy.core import GameCore

        self.hp -= damage
        if self.hp <= 0:
            self.remove = True

        # if self.hp <= 0 and self in GameCore.instance.objects:
            # GameCore.instance.objects.remove(self)
