# -*- coding: utf8 -*-


class Shield(object):
    power = 100
    max_power = 100
    energy_use = 1
    pps = 1  # power per step

    ship = None

    def update(self):
        if self.power < self.max_power and self.ship.energy >= self.energy_use:
            self.ship.energy -= self.energy_use
            self.power = min(self.power + self.pps, self.max_power)
