# -*- coding: utf8 -*-
from gamepy.drawable import Drawable
from gamepy.controllable import Controllable
from pygame.locals import K_UP, K_DOWN, K_LEFT, K_RIGHT, K_SPACE
from zyrian.locals import PLAYERS
from zyrian import g


RIGHT, UP, LEFT, DOWN, SHOOT = range(5)


class Ship(Drawable, Controllable):
    buttons = {
        K_RIGHT: RIGHT,
        K_UP: UP,
        K_LEFT: LEFT,
        K_DOWN: DOWN,
        K_SPACE: SHOOT
    }

    loop_animation = False

    speed = 100
    armor = 100
    side = PLAYERS

    idle_sprite = None
    left_sprite = None
    right_sprite = None

    _generator = None
    _shield = None

    def __init__(self, core):
        super(Ship, self).__init__()
        self.sprites = core.storage.sprites

        self.weapons = []

    def update(self, core):
        super(Ship, self).update(core)

        self._generator.update()
        self._shield.update()
        for w in self.weapons:
            w.update()

        if self.keys[RIGHT] and self.x < core.screen_width:
            self.x += self.speed * g.deltatime
        if self.keys[LEFT] and self.x > 0:
            self.x -= self.speed * g.deltatime

        if self.keys[UP] and self.y > 0:
            self.y -= self.speed * g.deltatime
        if self.keys[DOWN] and self.y < core.screen_height:
            self.y += self.speed * g.deltatime

        if self.keys[SHOOT]:
            for w in self.weapons:
                w.shoot()

    def key_down(self, key):
        key = super(Ship, self).key_down(key)
        if key == RIGHT:
            self.sprite = self.right_sprite
        elif key == LEFT:
            self.sprite = self.left_sprite
            
    def key_up(self, key):
        super(Ship, self).key_up(key)
        if not self.keys[RIGHT] and not self.keys[LEFT]:
            self.sprite = self.idle_sprite

    def hit(self, damage):
        self.armor -= damage

    @property
    def energy(self):
        return self.generator.energy

    @energy.setter
    def energy(self, value):
        self.generator.energy = value

    def add_weapon(self, weapon):
        weapon.ship = self
        self.weapons.append(weapon)

    @property
    def generator(self):
        return self._generator

    @generator.setter
    def generator(self, g):
        g.ship = self
        self._generator = g

    @property
    def shield(self):
        return self._shield

    @shield.setter
    def shield(self, s):
        s.ship = self
        self._shield = s
