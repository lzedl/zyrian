# -*- coding: utf8 -*-
from gamepy.drawable import Drawable


class Projectile(Drawable):
    damage = 0
    x_speed = 0
    y_speed = 0
    side = None
    lifetime = -1

    def __init__(self, side, x, y, w, h, sprite):
        super(Projectile, self).__init__(x, y, w, h, sprite)
        self.side = side

    def update(self, core):
        super(Projectile, self).update(core)
        self.x += self.x_speed
        self.y += self.y_speed
        for obj in core.collisions_for(self):
            if hasattr(obj, 'side') and self.side != obj.side:
                obj.hit(self.damage)
                self.remove = True
                # core.objects.remove(self)
                return

        if self.lifetime > 0:
            self.lifetime -= 1

        if self.lifetime == 0:
            self.remove = True
            # core.objects.remove(self)
            return

        if (self.x < -20 or self.x > core.screen_width + 20 or
           self.y < -20 or self.y > core.screen_height + 20):
            # core.objects.remove(self)
            self.remove = True
            return


class Weapon(object):
    cooldown = 0
    max_cooldown = 10
    _shoot_func = None
    energy_use = 0

    level = 1

    ship = None

    def __init__(self, level=1):
        self.set_level(level)

    def update(self):
        if self.cooldown > 0:
            self.cooldown -= 1

    def shoot(self):
        if (self._shoot_func and self.cooldown == 0
           and self.ship.energy >= self.energy_use):
            self._shoot_func()
            self.cooldown = self.max_cooldown

    def set_level(self, level):
        if hasattr(self, 'shoot_%d' % level):
            self.level = level
            self._shoot_func = getattr(self, 'shoot_%d' % level)
