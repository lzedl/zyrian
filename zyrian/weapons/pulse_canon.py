# -*- coding: utf8 -*-
from zyrian.weapons import Weapon, Projectile
from gamepy.core import GameCore


class PulseBullet(Projectile):
    damage = 2
    y_speed = -15

    def __init__(self, ship, x, y):
        super(PulseBullet, self).__init__(ship, x, y, w=10, h=10,
                                          sprite='pulse-bullet')


class PulseCanon(Weapon):
    max_cooldown = 4

    def shoot_1(self):
        GameCore.instance.objects.append(
            PulseBullet(self.ship.side, self.ship.x, self.ship.y-5)
        )

    def shoot_2(self):
        GameCore.instance.objects.append(
            PulseBullet(self.ship.side, self.ship.x-5, self.ship.y-5)
        )
        GameCore.instance.objects.append(
            PulseBullet(self.ship.side, self.ship.x+5, self.ship.y-5)
        )

    def shoot_3(self):
        GameCore.instance.objects.append(
            PulseBullet(self.ship.side, self.ship.x-5, self.ship.y)
        )
        GameCore.instance.objects.append(
            PulseBullet(self.ship.side, self.ship.x+5, self.ship.y)
        )

        GameCore.instance.objects.append(
            PulseBullet(self.ship.side, self.ship.x-15, self.ship.y)
        )
        GameCore.instance.objects.append(
            PulseBullet(self.ship.side, self.ship.x+15, self.ship.y)
        )
