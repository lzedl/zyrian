# -*- coding: utf8 -*-


class Generator(object):
    energy = 100
    max_energy = 100
    eps = 1

    def update(self):
        self.energy = min(self.energy + self.eps, self.max_energy)
