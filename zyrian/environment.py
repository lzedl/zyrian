# -*- coding: utf8 -*-
from gamepy.drawable import Drawable
from zyrian.enemies.asteroid import Asteroid
from random import randint
from zyrian import g


class Environment(Drawable):
    back_speed = 100

    def __init__(self):
        from gamepy.core import GameCore

        super(Environment, self).__init__(
            sprite=GameCore.instance.storage.sprites['space-back']
        )
        self.asteroid_timer = randint(15, 30)

    def update(self, core):
        self.y += self.back_speed * g.deltatime
        if self.y > self.sprite.h:
            self.y -= self.sprite.h

        self.asteroid_timer -= 1
        if self.asteroid_timer <= 0:
            a = Asteroid(randint(0, core.screen_width), -30, randint(1, 3))
            core.objects.append(a)
            self.asteroid_timer = randint(0, 200)

    def _draw_sprite(self, renderer):
        self.sprite.draw_on(renderer.screen, self.x,
                            self.y - self.sprite.h, 0)
        self.sprite.draw_on(renderer.screen, self.x, self.y, 0)
