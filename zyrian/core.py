# -*- coding: utf8 -*-
from gamepy.core import GameCore
from zyrian.weapons.pulse_canon import PulseCanon
from zyrian.storage import ZyrianStorage
from zyrian.environment import Environment


class Zyrian(GameCore):
    caption = 'Zyrian'
    storage_class = ZyrianStorage
    debug = False

    def run(self, network=False):
        self.objects.append(Environment())

        o = self.storage.create_ship('USP Talon',
                                     weapons=[PulseCanon(2)],
                                     generator='Standard MR-9',
                                     shield='Structural Integrity Field')
        o.x = 50
        o.y = 50

        self.objects.append(o)

        self.events.target = o

        from zyrian.enemies.asteroid import Asteroid

        # for i in xrange(1, 4):
        #     a = Asteroid(60 + 100*i, 30, i)
        #     self.objects.append(a)

        super(Zyrian, self).run()
