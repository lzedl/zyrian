# -*- coding: utf8 -*-
from gamepy.storage import DataStorage
from zyrian.ship import Ship
from zyrian.generators import Generator
from zyrian.shields import Shield
from os.path import join
import yaml


class ZyrianStorage(DataStorage):
    def __init__(self, core):
        super(ZyrianStorage, self).__init__(core)
        self.ships_data = {}
        self.generators_data = {}
        self.shields_data = {}

    def load_data(self, dir_path):
        super(ZyrianStorage, self).load_data(dir_path)

        with open(join(dir_path, 'ships.yml')) as fd:
            self.ships_data = yaml.load(fd)

        with open(join(dir_path, 'generators.yml')) as fd:
            self.generators_data = yaml.load(fd)

        with open(join(dir_path, 'shields.yml')) as fd:
            self.shields_data = yaml.load(fd)

    def create_ship(self, name, weapons, generator, shield):
        meta = self.ships_data[name]

        ship = Ship(self.core)
        for weapon in weapons:
            ship.add_weapon(weapon)
        ship.generator = self.create_generator(generator)
        ship.shield = self.create_shield(shield)

        ship.armor = meta['armor']
        ship.speed = meta['speed']
        ship.w = meta['width']
        ship.h = meta['height']
        ship.idle_sprite = self.sprites[meta['idle_sprite']]
        ship.left_sprite = self.sprites[meta['left_sprite']]
        ship.right_sprite = self.sprites[meta['right_sprite']]
        ship.sprite = ship.idle_sprite

        return ship

    def create_generator(self, name):
        meta = self.generators_data[name]
        generator = Generator()
        generator.energy = generator.max_energy = meta['max_energy']
        generator.eps = meta['eps']

        return generator

    def create_shield(self, name):
        meta = self.shields_data[name]
        shield = Shield()

        shield.max_power = shield.power = meta['max_power']
        shield.energy_use = meta['energy_use']
        shield.pps = meta['pps']

        return shield
