# -*- coding: utf8 -*-
import logging.config
import logging
import yaml


try:  # Python 2.7+
    from logging import NullHandler
except ImportError:
    class NullHandler(logging.Handler):
        def emit(self, record):
            pass
