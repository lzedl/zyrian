# -*- coding: utf8 -*-
from gamepy.render import Render
from gamepy.events import EventManager, Exit
from gamepy.storage import DataStorage

import pygame
from pygame.draw import rect
from zyrian import g
from os.path import dirname, join, abspath
import logging


logger = logging.getLogger(__name__)


class GameCore(object):
    instance = None

    pause = 30
    step = 0
    last_update = 0
    frame_rate = 40

    render_class = Render
    events_class = EventManager
    storage_class = DataStorage

    screen_width = 160 * 4
    screen_height = 90 * 4

    caption = 'GamePy'

    data_dir = abspath(join(dirname(__file__), '..', 'data'))

    debug = False

    def __init__(self):
        GameCore.instance = self
        self.render = self.render_class(self.caption)
        self.events = self.events_class()
        self.storage = self.storage_class(core=self)

        self.render.init_screen(self.screen_width, self.screen_height)
        logger.info('Data dir: %s', self.data_dir)
        self.storage.load_data(self.data_dir)

        self.clock = pygame.time.Clock()

        # self.gui = GUI()
        self.objects = []

    def main_loop(self):
        self.step = 0
        objects = self.objects
        clock = pygame.time.Clock()
        try:
            while True:
                start_time = pygame.time.get_ticks();
                self.step += 1
                self.events.update()
                self.render.clear()

                for o in self.objects:
                    o.update(self)

                for o in self.objects:
                    o.draw(self.render)
                    if self.debug:
                        rect(self.render.screen, (0, 255, 0),
                             (o.x - o.w/2, o.y - o.h/2, o.w, o.h), 1)

                n = len(objects) - 1
                while n >= 0:
                    if objects[n].remove:
                        del objects[n]
                    n -= 1
                # self.objects[:] = ifilterfalse(attrgetter('remove'),
                #                                self.objects)

                g.deltatime = (pygame.time.get_ticks() - start_time) / 1000.0;
                #logger.error('Sleep: %f', g.deltatime)

                clock.tick()
                self.render.flip()
        except Exit:
            return

    def run(self, network=False):
        self.main_loop()

    def collisions_for(self, obj):
        for o in self.objects:
            if (obj != o and
               abs(o.x - obj.x) < (o.w + obj.w)/2 and
               abs(o.y - obj.y) < (o.h + obj.h)/2):
                yield o
