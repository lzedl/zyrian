from blinker import Signal
from pygame.locals import *
import pygame


class Exit(Exception):
    pass


class EventManager(object):
    key_down = Signal()
    key_up = Signal()
    player_key_down = Signal()
    player_key_up = Signal()
    _target = None

    def __init__(self):
        self.keys = [False] * K_LAST
        self.player_keys = {}

    def set_player_keys(self, settings):
        self.player_keys = settings

    def update(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                raise Exit()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    raise Exit()
                else:
                    self.keys[event.key] = True
                    self.key_down.send(event.key)
                    # if event.key in self.player_keys:
                    #     action_key = self.player_keys[event.key]
                    #     self.player_key_down.send(action_key)
                    #     if self._target:
                    #         self._target.key_down(action_key)
                    if self._target:
                        self._target.key_down(event.key)
            elif event.type == KEYUP:
                self.keys[event.key] = False
                self.key_up.send(event.key)
                # if event.key in self.player_keys:
                #     action_key = self.player_keys[event.key]
                #     self.player_key_up.send(action_key)
                #     if self._target:
                #         self._target.key_up(action_key)
                if self._target:
                    self._target.key_up(event.key)
            elif event.type == JOYBUTTONDOWN:
                pass
            elif event.type == JOYAXISMOTION:
                pass

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, value):
        """Set controlled target

        :param Controllable value: target to control
        """
        # if self._target:
        #     for action_key, code in self.player_keys.iteritems():
        #         if self.keys[code]:
        #             self._target.key_up(action_key)

        self._target = value
