# -*- coding: utf8 -*-


class Controllable(object):
    """Класс, который отображает объекты, которые могут быть
    контролируемы посредством устройств ввода: клавиатурой или геймпадом

    """
    buttons = {}

    def __init__(self):
        super(Controllable, self).__init__()
        self.keys = {k: False for k in xrange(len(self.buttons))}

    def key_down(self, key):
        key = self.buttons.get(key)
        if key is not None:
            self.keys[key] = True
            return key

    def key_up(self, key):
        key = self.buttons.get(key)
        if key is not None:
            self.keys[key] = False
            return  key
