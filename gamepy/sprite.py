# -*- coding: utf8 -*-
import pygame


class Sprite(object):
    x, y = 0, 0  # center
    w, h = 0, 0
    frames = 1
    pause = 0
    surface = None
    cols, rows = 1, 1

    def __init__(self):
        self.rpos = [0, 0]
        self.ra = [0, 0, 32, 32]

    @classmethod
    def create_empty(cls, w, h):
        s = cls()
        s.surface = pygame.Surface((w, h))
        s.set_frame_size(w, h)
        return s

    @classmethod
    def load_tile(cls, filename):
        s = cls()
        s.surface = pygame.image.load(filename).convert_alpha()
        s.cols = 4
        s.set_frame_size(32, 32)
        s.rows = s.surface.get_width() // 32
        s.frames = s.rows * 4
        return s

    @classmethod
    def from_file(cls, filename, frames=1, x=0, y=0, cols=None, pause=0):
        s = cls()
        s.frames = frames
        s.pause = pause
        s.surface = pygame.image.load(filename).convert_alpha()

        if cols is None:
            s.cols = frames
            s.rows = 1
        else:
            s.cols = cols
            s.rows = (frames - 1) // cols + 1

        s.set_frame_size(s.surface.get_width() // s.cols,
                         s.surface.get_height() // s.rows)

        s.x = x
        s.y = y

        return s

    def set_frame_size(self, w, h):
        self.w = w
        self.h = h
        self.ra = [0, 0, w, h]

    def draw_on(self, surface, x, y, frame):
        row = frame // self.cols
        col = frame - row * self.cols
        self.rpos[0] = x - self.x
        self.rpos[1] = y - self.y
        self.ra[0] = self.w * col
        self.ra[1] = self.h * row
        surface.blit(self.surface, self.rpos, self.ra)

    def draw_part_on(self, surface, x, y, rect):
        self.rpos[0] = x - self.x
        self.rpos[1] = y - self.y
        surface.blit(self.surface, self.rpos, rect)
