# -*- coding: utf8 -*-
from gamepy.sprite import Sprite
import logging


logger = logging.getLogger(__name__)


class Drawable(object):
    """Базовый объект, который может отрисовывать себя.

    """
    x, y = 0, 0
    w, h = 0, 0
    depth = 0
    frame = 0
    _sprite = None
    parts = None
    last_update = 0
    loop_animation = True
    remove = False

    def __init__(self, x=0, y=0, w=0, h=0, sprite=None):
        super(Drawable, self).__init__()
        self.x, self.y = x, y
        self.w, self.h = w, h
        self.sprite = sprite

    def update(self, core):
        if core.step - self.last_update > self.sprite.pause:
            if self.frame + 1 >= self.sprite.frames:
                if self.loop_animation:
                    self.frame = 0
                    self.last_update = core.step
            else:
                self.frame += 1
                self.last_update = core.step

    def set_sprite(self, sprite):
        """Указать какой спрайт использовать

        :param Sprite|SpriteSet sprite: спрайт или набор спрайтов
        """
        from gamepy.core import GameCore

        self.frame = 0
        self.last_update = GameCore.instance.step
        self._sprite = sprite
        if isinstance(sprite, Sprite):
            self.draw = self._draw_sprite
        elif isinstance(sprite, basestring):
            if sprite in GameCore.instance.storage.sprites:
                self.sprite = GameCore.instance.storage.sprites[sprite]
            else:
                logger.warning('Sprite "%s" not found', sprite)
        else:
            self.draw = self._draw_empty

    @property
    def sprite(self):
        return self._sprite

    @sprite.setter
    def sprite(self, value):
        self.set_sprite(value)

    def _draw_sprite(self, render):
        self._sprite.draw_on(render.screen, self.x, self.y, self.frame)

    def _draw_empty(self, screen):
        pass

    draw = _draw_empty
