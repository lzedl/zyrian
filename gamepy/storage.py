# -*- coding: utf8 -*-
from gamepy.sprite import Sprite
from gamepy.utils import split_suffix

from collections import defaultdict
from functools import wraps
from os.path import exists, join, basename
from glob import glob
import yaml


def load_meta_info(func):
    @wraps(func)
    def wrapper(self, dir_path):
        meta = defaultdict(dict)
        meta_path = join(dir_path, 'meta.yml')
        if exists(meta_path):
            with open(meta_path) as fd:
                meta.update(yaml.load(fd))
        
        return func(self, dir_path, meta)

    return wrapper


class DataStorage(object):
    def __init__(self, core):
        self.core = core
        self.sprites = {}
        self.music = {}
        self.sfx = {}

    def load_data(self, dir_path):
        if exists(join(dir_path, 'sprites')):
            self.load_sprites(join(dir_path, 'sprites'))

        if exists(join(dir_path, 'music')):
            self.load_sprites(join(dir_path, 'music'))

        if exists(join(dir_path, 'sfx')):
            self.load_sprites(join(dir_path, 'sfx'))

    @load_meta_info
    def load_sprites(self, dir_path, meta=None):
        for filename in glob(join(dir_path, '*.png')):
            name, _ = split_suffix(basename(filename))
            meta_item = meta[name]
            self.sprites[name] = Sprite.from_file(
                filename=filename,
                frames=meta_item.get('frames', 1),
                x=meta_item.get('x', 0),
                y=meta_item.get('y', 0),
                cols=meta_item.get('cols'),
                pause=meta_item.get('pause', 0)
            )

    @load_meta_info
    def load_sfx(self, dir_name, meta):
        pass

    @load_meta_info
    def load_music(self, dir_name, meta):
        pass
