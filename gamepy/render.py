# -*- coding: utf8 -*-
from pygame.locals import *
import pygame

from pygame.font import Font, get_default_font


class Render(object):
    """Класс, через который необходимо все рисовать.
    """
    w, h = None, None
    screen = None

    def __init__(self, caption):
        super(Render, self).__init__()
        pygame.init()
        pygame.display.set_caption(caption)
        # self.font = Font(get_default_font(), 10)
        self.default_font = Font(get_default_font(), 12)

    def configure(self, width, height):
        self.init_screen(width, height)

    def init_screen(self, width, height):
        self.w = width
        self.h = height
        flags = DOUBLEBUF
        # flags = 0
        # flags = HWSURFACE | HWACCEL | DOUBLEBUF | ASYNCBLIT
        self.screen = pygame.display.set_mode((width, height), flags)

    def clear(self):
        self.screen.fill((0, 0, 0))

    def flip(self):
        pygame.display.flip()
